import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../not-found/not-found.component';
import { ManagerAuthGuard } from '../shared/guards/manager-auth.guard';
import { CreatePollComponent } from './create-poll/create-poll.component';
import { PollsListComponent } from './polls-list/polls-list.component';

const routes: Routes = [
  { path: '',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: PollsListComponent
  },
  { path: 'create',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: CreatePollComponent
  },
  { path: '{poll_id}',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: NotFoundComponent
  },
  { path: '{poll_id}/edit',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: NotFoundComponent
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PollsRoutingModule { }
