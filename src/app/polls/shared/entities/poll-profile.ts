import { User } from "src/app/users/shared/entities/user";

export interface PollProfile {
  id: number;
  general: PollGeneral;
  cityHall: PollParty[];
  cityCouncil: PollParty[];
  parishes: PollParish[];
}

export interface PollGeneral {
  name: string;
  date: Date;
  timeStart: string;
  timeEnd: string;
}

export interface PollParty {
  id?: number;
  name: string;
  short: string;
  primary: boolean;
}

export interface PollParish {
  id?: number;
  name: string;
  short: string;

  tables: PollParishTable[];
  parties: PollParty[];
}

export interface PollParishTable {
  id?: number;
  tableNumber: number;
  responsibles: User[];
}
