import { BasicEntity } from "src/app/shared/entities/basic-entity";

export interface Poll extends BasicEntity {
  date: Date;
}
