import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Observable, of, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/helpers/confirm-dialog/confirm-dialog.component';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { PollsService } from 'src/app/shared/services/polls.service';
import { Filter } from 'src/app/shared/util/filter';
import { Poll } from '../shared/entities/poll';

@Component({
  selector: 'app-polls-list',
  templateUrl: './polls-list.component.html',
  styleUrls: ['./polls-list.component.scss']
})
export class PollsListComponent implements OnInit {

  public autoCompletePolls: Observable<BasicEntity[]> = of([]);

  public pollsDataSource: Observable<Poll[]> = of([]);

  public displayedColumns: string[] = [
    'name', 'date', 'actions'
  ];

  public filterColumns: string[] = this.displayedColumns.map(col => 'filter' + col);

  public formControlNameFilter: FormControl = new FormControl();

  public formControlDateStartFilter: FormControl = new FormControl();

  public formControlDateEndFilter: FormControl = new FormControl();

  public totalPolls: number = 0;

  public filterByName: Filter = { name: "name", value: '' };

  public filterByDateStart: Filter = { name: "dateStart", value: '' };

  public filterByDateEnd: Filter = { name: "dateEnd", value: '' };

  public pageIndex: Filter = { name: "page", value: 0 };

  public itemsPerPage: Filter = { name: "itemsPerPage", value: 10 };

  public sortBy: Filter = { name: "sortBy", value: 'name' };

  public sortDirection: Filter = { name: "sortDir", value: 'asc' };

  private filterNameSubscription$?: Subscription;

  private filterDateStartSubscription$?: Subscription;

  private filterDateEndSubscription$?: Subscription;

  public constructor(
    private readonly pollsService: PollsService,
    private readonly dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    this.updateTableData();
    this.subscribeFilterNameChanges();
    this.subscribeFilterDateChanges();
  }

  public ngOnDestroy(): void {
    this.filterNameSubscription$?.unsubscribe();
    this.filterDateStartSubscription$?.unsubscribe();
    this.filterDateEndSubscription$?.unsubscribe();
  }

  private subscribeFilterNameChanges(): void {
    this.filterNameSubscription$ = this.formControlNameFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this.setAutocompleteFilter(value, this.filterByName)),
        switchMap(value => this.updateNameFilter()),
        map((result: any) => {
          this.autoCompletePolls = of(result);
        })
      )
      .subscribe();
  }

  private subscribeFilterDateChanges(): void {
    this.filterDateStartSubscription$ = this.formControlDateStartFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          this.filterByDateStart.value = value;
          this.updateTableData()
        })
      )
      .subscribe();

    this.filterDateEndSubscription$ = this.formControlDateEndFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          this.filterByDateEnd.value = value;
          this.updateTableData()
        })
      )
      .subscribe();
  }

  private setAutocompleteFilter(value: string | BasicEntity, filter: Filter): string | BasicEntity {
    filter.value = (typeof value === 'string') ? value : value?.name;
    this.updateTableData();

    return value;
  }

  private updateNameFilter(): Observable<BasicEntity> {
    return this.pollsService.getFilterPolls(this.filterByName);
  }

  public changeSort($sort: Sort) {
    this.sortBy.value = $sort.active;
    this.sortDirection.value = $sort.direction;
    this.updateTableData();
  }

  public changePage($page: PageEvent) {
    this.pageIndex.value = $page.pageIndex;
    this.itemsPerPage.value = $page.pageSize;
    this.updateTableData();
  }

  public updateTableData() {
    const pollsListSubscription = this.pollsService
      .getPolls(this.pollsListFilters())
      .subscribe({
        next: results => {
          this.pollsDataSource = of(results.list);
          this.totalPolls = results.total;
        },
        complete: () => { pollsListSubscription.unsubscribe() }
      });
  }

  private pollsListFilters(): Filter[] {
    const filters = [];

    filters.push(this.pageIndex);
    filters.push(this.itemsPerPage);
    filters.push(this.sortBy);
    filters.push(this.sortDirection);
    filters.push(this.filterByName);
    filters.push(this.filterByDateStart);
    filters.push(this.filterByDateEnd);

    return filters;
  }

  public confirmPollRemove(poll: Poll) {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        disableClose: true,
        data: {
          title: 'Eliminar eleição',
          body: 'Tem a certeza de que pretende eliminar a eleição: ' + poll.name + '?',
          okButtonAction: () => {
            this.removePoll(poll, dialogRef);
          }
        }
      }
    );
  }

  private removePoll(poll: Poll, dialog: MatDialogRef<ConfirmDialogComponent>) {
    const removePollSubscription$ = this.pollsService
      .deletePoll(poll.id)
      .subscribe({
        next: (result: any) => {
          dialog.close();
          this.updateTableData();
        },
        complete: () => { removePollSubscription$.unsubscribe() }
      });
  }

}
