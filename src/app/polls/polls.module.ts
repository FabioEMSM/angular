import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { AutocompleteFilterModule } from '../autocomplete-filter/autocomplete-filter.module';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

import { PollsRoutingModule } from './polls-routing.module';
import { PollsListComponent } from './polls-list/polls-list.component'
import { CreatePollComponent } from './create-poll/create-poll.component';
import { PollFormComponent } from './poll-form/poll-form.component';
import { PollFormPartiesComponent } from './poll-form/poll-form-parties/poll-form-parties.component';
import { PollFormParishAssemblyComponent } from './poll-form/poll-form-parish-assembly/poll-form-parish-assembly.component';
import { PollFormGeneralComponent } from './poll-form/poll-form-general/poll-form-general.component';
import { PollFormTablesComponent } from './poll-form/poll-form-tables/poll-form-tables.component';
import { PollFormUsersComponent } from './poll-form/poll-form-users/poll-form-users.component';

@NgModule({
  declarations: [
    PollsListComponent,
    CreatePollComponent,
    PollFormComponent,
    PollFormPartiesComponent,
    PollFormParishAssemblyComponent,
    PollFormGeneralComponent,
    PollFormTablesComponent,
    PollFormUsersComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatPaginatorModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatDialogModule,
    MatSelectModule,

    PollsRoutingModule,
    AutocompleteFilterModule,
  ]
})
export class PollsModule { }
