import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Parish } from 'src/app/parishes/shared/entities/parish';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { controlHasErrors } from 'src/app/shared/helper';
import { ParishesService } from 'src/app/shared/services/parishes.service';

@Component({
  selector: 'app-poll-form-parish-assembly',
  templateUrl: './poll-form-parish-assembly.component.html',
  styleUrls: ['./poll-form-parish-assembly.component.scss']
})
export class PollFormParishAssemblyComponent implements OnInit, OnDestroy {

  public subPanels: {
    tables?: number,
    parties?: number
  } = {};

  public openedPartiesDefinition?: number;

  public formArray?: FormArray;

  public filterOptions: Observable<BasicEntity[]> = of([]);

  private filterShortSubscriptions$: Subscription[] = [];

  private filterNameSubscriptions$: Subscription[] = [];

  public constructor(
    private readonly parishesService: ParishesService,
    private readonly controlContainer: ControlContainer
  ) { }

  public ngOnInit(): void {
    if (this.controlContainer.control instanceof FormArray) {
      this.formArray = this.controlContainer.control as FormArray;
      this.subscribeExistingFormGroups();
    }
    else {
      console.error('A FormArray control is required');
    }
  }

  public ngOnDestroy(): void {
    this.filterShortSubscriptions$.forEach(subs => subs.unsubscribe());
    this.filterNameSubscriptions$.forEach(subs => subs.unsubscribe());
  }

  private subscribeExistingFormGroups(): void {
    for(const formGroup of this.formArray?.controls || []) {
      this.addShortSubscription((formGroup as FormGroup).controls.short as FormControl);
      this.addNameSubscription((formGroup as FormGroup).controls.name as FormControl);
    }
  }

  public castToformGroup(abstractControl: AbstractControl): FormGroup {
    return abstractControl as FormGroup;
  }

  public hasErrors(formControl: AbstractControl, errors?: string[]): boolean {
    return controlHasErrors(formControl, errors);
  }

  private closeSubPanels() {
    this.subPanels.tables = undefined;
    this.subPanels.parties = undefined;
  }

  public toggleSubPanel(panelIndex: number, type: 'tables' | 'parties') {
    const isOpened = this.subPanels[type] === panelIndex;
    this.closeSubPanels();

    if (!isOpened) {
      this.subPanels[type] = panelIndex;
    }
  }

  public addParish(): void {
    // Close subpanels
    this.closeSubPanels();

    // Create FormControl for "short" field and set subscription
    const short = new FormControl(
      '',
      [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32)
      ]
    );
    this.addShortSubscription(short);

    // Create FormControl for "name" field and set subscription
    const name = new FormControl(
      '',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]
    );
    this.addNameSubscription(name);

    // Create a FormGroup and add to list
    this.formArray?.push(
      new FormGroup({
        short,
        name,
        mandates: new FormControl(
          0,
          [
            Validators.required,
            Validators.pattern(/^[0-9]+$/)
          ]
        ),
        parties: new FormArray([]),
        tables: new FormArray([])
      })
    );
  }

  public addShortSubscription(formControl: FormControl): void {
    this.filterShortSubscriptions$.push(
      formControl.valueChanges
        .pipe(
          startWith(''),
          switchMap(value => this.updateNameFilter(value)),
          map((result: Parish[]) => {
            return this.filterOptions = of(result);
          })
        )
        .subscribe()
    );
  }

  public addNameSubscription(formControl: FormControl): void {
    this.filterNameSubscriptions$.push(
      formControl.valueChanges
        .pipe(
          startWith(''),
          switchMap(value => this.updateNameFilter(value)),
          map((result: Parish[]) => {
            return this.filterOptions = of(result);
          })
        )
        .subscribe()
    );
  }

  private updateNameFilter(search: string): Observable<Parish[]> {
    return this.parishesService.getFilterParishes([{name: 'name', value: search}]);
  }

  public removeParish(index: number): void {
    // Unsubscribe changes
    this.filterShortSubscriptions$[index].unsubscribe();
    this.filterNameSubscriptions$[index].unsubscribe();

    // Remove subscriptions
    this.filterShortSubscriptions$.splice(index, 1);
    this.filterNameSubscriptions$.splice(index, 1);

    // Remove FormGroup from list
    this.formArray?.removeAt(index);
  }

  public displayFunctionShort(
    data: {option: Parish, formGroup: FormGroup} | string
  ): string {
    if (typeof data !== 'string') {
      (data.formGroup.controls.name as FormControl).setValue(
        data.option.name, { emitEvent: false }
      );

      // Workaroud to set the contents of this formControl to
      // string instead of the [data] object
      setTimeout(() => {
        (data.formGroup.controls.short as FormControl).setValue(
          data.option.short, { emitEvent: false }
        );
      }, 0);

      return data.option.short;
    }

    return data;
  }

  public displayFunctionName(
    data: {option: Parish, formGroup: FormGroup} | string
  ): string {
    if (typeof data !== 'string') {
      (data.formGroup.controls.short as FormControl).setValue(
        data.option.short, { emitEvent: false }
      );

      // Workaroud to set the contents of this formControl to
      // string instead of the [data] object
      setTimeout(() => {
        (data.formGroup.controls.name as FormControl).setValue(
          data.option.name, { emitEvent: false }
        );
      }, 0);

      return data.option.name;
    }

    return data;
  }

}
