import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollFormParishAssemblyComponent } from './poll-form-parish-assembly.component';

describe('PollFormParishAssemblyComponent', () => {
  let component: PollFormParishAssemblyComponent;
  let fixture: ComponentFixture<PollFormParishAssemblyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollFormParishAssemblyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollFormParishAssemblyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
