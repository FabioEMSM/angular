import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { controlHasErrors } from 'src/app/shared/helper';
import { PollsService } from 'src/app/shared/services/polls.service';
import { UniqueValidator } from 'src/app/shared/validators/unique.validator';

@Component({
  selector: 'app-poll-form',
  templateUrl: './poll-form.component.html',
  styleUrls: ['./poll-form.component.scss']
})
export class PollFormComponent implements OnInit {
  // TODO: Prevent submit on keypress enter

  public isNewPoll: boolean = true;

  public formGroup?: FormGroup;

  public constructor(
    private readonly pollsService: PollsService
  ) { }

  public ngOnInit(): void {
    this.formGroup = new FormGroup({
      general: new FormGroup({
        name: new FormControl(
          '',
          [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(255)
          ],
          UniqueValidator.notUnique(this.pollsService, this.pollsService.checkPollUniqueness)
        ),
        date: new FormControl(
          new Date(),
          Validators.required
        ),
        timeStart: new FormControl(
          '08:00',
          [
            Validators.required,
            Validators.pattern(/([0-1][0-9]|2[0-3]):[0-5][0-9]/)
          ]
        ),
        timeEnd: new FormControl(
          '19:00',
          [
            Validators.required,
            Validators.pattern(/([0-1][0-9]|2[0-3]):[0-5][0-9]/)
          ]
        )
      }),
      cityHall: new FormGroup({
        mandates: new FormControl(
          11,
          [
            Validators.required,
            Validators.min(0)
          ]
        ),
        parties: new FormArray(
          [],
          {updateOn: 'change'}
        )
      }),
      cityCouncil: new FormGroup({
        mandates: new FormControl(
          33,
          [
            Validators.required,
            Validators.pattern(/^[0-9]+$/)
          ]
        ),
        parties:  new FormArray(
          []
        )
      }),
      parishAssemblies: new FormArray(
        []
      )
    });
  }

  public hasErrors(formControl: AbstractControl, errors?: string[]) {
    return controlHasErrors(formControl, errors);
  }

  public castToFormGroup(abstractControl: AbstractControl): FormGroup {
    return abstractControl as FormGroup
  }

  public castToFormArray(abstractControl: AbstractControl): FormArray {
    return abstractControl as FormArray
  }

  public submitForm(): void {
    console.log(this.formGroup?.value);
  }

  public cancelForm(): void {
  }

}
