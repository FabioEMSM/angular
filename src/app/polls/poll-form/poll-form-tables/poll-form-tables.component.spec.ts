import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollFormTablesComponent } from './poll-form-tables.component';

describe('PollFormTablesComponent', () => {
  let component: PollFormTablesComponent;
  let fixture: ComponentFixture<PollFormTablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollFormTablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollFormTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
