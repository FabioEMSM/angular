import { Component, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PollFormUsersComponent } from '../poll-form-users/poll-form-users.component';

@Component({
  selector: 'app-poll-form-tables',
  templateUrl: './poll-form-tables.component.html',
  styleUrls: ['./poll-form-tables.component.scss']
})
export class PollFormTablesComponent implements OnInit {

  public formArray?: FormArray;

  public constructor(
    public readonly controlContainer: ControlContainer,
    public readonly dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    if (this.controlContainer.control instanceof FormArray) {
      this.formArray = this.controlContainer.control as FormArray;
    }
    else {
      console.error('A FormArray control is required');
    }
  }

  public castToformGroup(formGroup: AbstractControl): FormGroup {
    return formGroup as FormGroup;
  }

  public castToFormArray(abstractControl: AbstractControl): FormArray {
    return abstractControl as FormArray
  }

  public addTable() {
    // Create a FormGroup and add to list
    this.formArray?.push(
      new FormGroup({
        tableNumber: new FormControl(
          this.formArray.length + 1,
          [
            Validators.required
          ]
        ),
        users: new FormArray([])
      })
    );
  }

  public removeTable() {
    this.formArray?.removeAt(this.formArray.length - 1);
  }

  public changeResponsibles(formGroup: AbstractControl) {
    const parish = (formGroup.parent?.parent as FormGroup)?.controls.name.value;
    const tableNumber = (formGroup as FormGroup).controls.tableNumber.value;

    const dialogRef = this.dialog.open(
      PollFormUsersComponent,
      {
        disableClose: true,
        width: '720px',
        data: {
          parish,
          tableNumber,
          responsibles: (formGroup as FormGroup).controls.users
        }
      }
    );
  }

}
