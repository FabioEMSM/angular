import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollFormPartiesComponent } from './poll-form-parties.component';

describe('PollFormPartiesComponent', () => {
  let component: PollFormPartiesComponent;
  let fixture: ComponentFixture<PollFormPartiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollFormPartiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollFormPartiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
