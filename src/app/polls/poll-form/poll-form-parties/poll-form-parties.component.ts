import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { PartyFilter } from 'src/app/parties/shared/entities/party-filter';
import { controlHasErrors } from 'src/app/shared/helper';
import { PartiesService } from 'src/app/shared/services/parties.service';

@Component({
  selector: 'app-poll-form-parties',
  templateUrl: './poll-form-parties.component.html',
  styleUrls: ['./poll-form-parties.component.scss']
})
export class PollFormPartiesComponent implements OnInit, OnDestroy {

  public formArray?: FormArray;

  public filterOptions: Observable<PartyFilter[]> = of([]);

  private filterShortSubscriptions$: Subscription[] = [];

  private filterNameSubscriptions$: Subscription[] = [];

  public constructor(
    public readonly partiesService: PartiesService,
    public readonly controlContainer: ControlContainer
  ) { }

  public ngOnInit(): void {
    if (this.controlContainer.control instanceof FormArray) {
      this.formArray = this.controlContainer.control as FormArray;
      this.subscribeExistingFormGroups();
    }
    else {
      console.error('A FormArray control is required');
    }
  }

  public ngOnDestroy(): void {
    this.filterShortSubscriptions$.forEach(subs => subs.unsubscribe());
    this.filterNameSubscriptions$.forEach(subs => subs.unsubscribe());
  }

  private subscribeExistingFormGroups(): void {
    for(const formGroup of this.formArray?.controls || []) {
      this.addShortSubscription((formGroup as FormGroup).controls.short as FormControl);
      this.addNameSubscription((formGroup as FormGroup).controls.name as FormControl);
    }
  }

  public castToformGroup(formGroup: AbstractControl): FormGroup {
    return formGroup as FormGroup;
  }

  public hasErrors(formControl: AbstractControl, errors?: string[]) {
    return controlHasErrors(formControl, errors);
  }

  public addParty(): void {
    // Check if there is already a primary
    const hasNoPrimary = this.formArray?.controls
      .every((control: AbstractControl) => {
        !(control as FormGroup).controls.primary.value
      });

    // Create FormControl for "short" field and set subscription
    const short = new FormControl(
      '',
      [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32)
      ]
    );
    this.addShortSubscription(short);

    // Create FormControl for "name" field and set subscription
    const name = new FormControl(
      '',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]
    );
    this.addNameSubscription(name);

    // Create a FormGroup and add to list
    this.formArray?.push(
      new FormGroup({
        primary: new FormControl(
          hasNoPrimary
        ),
        short,
        name,
        color: new FormControl(
          '',
          [
            Validators.pattern(/^\#[0-9a-fA-F]{6,6}$/)
          ]
        ),
      })
    );
  }

  public addShortSubscription(formControl: FormControl): void {
    this.filterShortSubscriptions$.push(
      formControl.valueChanges
        .pipe(
          startWith(''),
          switchMap(value => this.updateNameFilter(value)),
          map((result: PartyFilter[]) => {
            return this.filterOptions = of(result);
          })
        )
        .subscribe()
    );
  }

  public addNameSubscription(formControl: FormControl): void {
    this.filterNameSubscriptions$.push(
      formControl.valueChanges
        .pipe(
          startWith(''),
          switchMap(value => this.updateNameFilter(value)),
          map((result: PartyFilter[]) => {
            return this.filterOptions = of(result);
          })
        )
        .subscribe()
    );
  }

  private updateNameFilter(search: string): Observable<PartyFilter[]> {
    return this.partiesService.getFilterParties([{name: 'name', value: search}]);
  }

  public changePrimary(index: number): void {
    // Mark all FormGroups as not primary
    this.formArray?.controls.forEach(control => {
      (control as FormGroup).controls.primary.setValue(false);
    });

    // Set only the FormGroup at index to primary
    (this.formArray?.controls[index] as FormGroup)
      .controls.primary.setValue(true);
  }

  public removeParty(index: number): void {
    // Unsubscribe changes
    this.filterShortSubscriptions$[index].unsubscribe();
    this.filterNameSubscriptions$[index].unsubscribe();

    // Remove subscriptions
    this.filterShortSubscriptions$.splice(index, 1);
    this.filterNameSubscriptions$.splice(index, 1);

    // Check if the FormGroup at index is marked as primary
    const isPrimary = (this.formArray?.controls[index] as FormGroup)
      .controls.primary.value;

    // Remove FormGroup from list
    this.formArray?.removeAt(index);

    // If the removed FormGroup was set as primary,
    // set the first FormGroup in the list to primary
    if (isPrimary) {
      (this.formArray?.controls[0] as FormGroup)
        ?.controls.primary.setValue(true);
    }
  }

  public displayFunctionShort(
    data: {option: PartyFilter, formGroup: FormGroup} | string
  ): string {
    if (typeof data !== 'string') {
      (data.formGroup.controls.name as FormControl).setValue(
        data.option.name, { emitEvent: false }
      );

      (data.formGroup.controls.color as FormControl).setValue(
        data.option.color, { emitEvent: false }
      );

      // Workaroud to set the contents of this formControl to
      // string instead of the [data] object
      setTimeout(() => {
        (data.formGroup.controls.short as FormControl).setValue(
          data.option.short, { emitEvent: false }
        );
      }, 0);

      return data.option.short;
    }

    return data;
  }

  public displayFunctionName(
    data: {option: PartyFilter, formGroup: FormGroup} | string
  ): string {
    if (typeof data !== 'string') {
      (data.formGroup.controls.short as FormControl).setValue(
        data.option.short, { emitEvent: false }
      );

      (data.formGroup.controls.color as FormControl).setValue(
        data.option.color, { emitEvent: false }
      );

      // Workaroud to set the contents of this formControl to
      // string instead of the [data] object
      setTimeout(() => {
        (data.formGroup.controls.name as FormControl).setValue(
          data.option.name, { emitEvent: false }
        );
      }, 0);

      return data.option.name;
    }

    return data;
  }

}
