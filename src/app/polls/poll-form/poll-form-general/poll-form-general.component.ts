import { Component, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormControl, FormGroup } from '@angular/forms';
import { controlHasErrors } from 'src/app/shared/helper';

@Component({
  selector: 'app-poll-form-general',
  templateUrl: './poll-form-general.component.html',
  styleUrls: ['./poll-form-general.component.scss']
})
export class PollFormGeneralComponent implements OnInit {

  public formGroup?: FormGroup;

  public constructor(
    private readonly controlContainer: ControlContainer
  ) { }

  public ngOnInit(): void {
    this.formGroup = this.controlContainer.control as FormGroup;
  }

  public hasErrors(formControl?: AbstractControl, errors?: string[]): boolean {
    return formControl ? controlHasErrors(formControl, errors) : false;
  }

}
