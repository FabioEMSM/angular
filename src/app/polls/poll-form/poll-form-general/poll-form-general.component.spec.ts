import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollFormGeneralComponent } from './poll-form-general.component';

describe('PollFormGeneralComponent', () => {
  let component: PollFormGeneralComponent;
  let fixture: ComponentFixture<PollFormGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollFormGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollFormGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
