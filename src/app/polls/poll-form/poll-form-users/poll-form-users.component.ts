import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { ConfirmDialogComponent } from 'src/app/helpers/confirm-dialog/confirm-dialog.component';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { controlHasErrors } from 'src/app/shared/helper';
import { UsersService } from 'src/app/shared/services/users.service';

export type PollFormUsersData = {
  parish: string,
  tableNumber: number,
  responsibles: FormArray
};

@Component({
  selector: 'app-poll-form-users',
  templateUrl: './poll-form-users.component.html',
  styleUrls: ['./poll-form-users.component.scss']
})
export class PollFormUsersComponent implements OnInit {

  public usersList: Observable<BasicEntity[]> = of([]);

  public constructor(
    private readonly usersService: UsersService,
    private readonly dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: PollFormUsersData,
  ) { }

  public ngOnInit(): void {
    const usersFilter$ = this.usersService.getFilterUsers([])
      .subscribe({
        next: result => {
          this.usersList = of(result);
        },
        complete: () => { usersFilter$.unsubscribe() }
      });
  }

  public castToformGroup(formGroup: AbstractControl): FormGroup {
    return formGroup as FormGroup;
  }

  public castToformControl(formControl: AbstractControl): FormControl {
    return formControl as FormControl;
  }

  public hasErrors(formControl: AbstractControl, errors?: string[]) {
    return controlHasErrors(formControl, errors);
  }

  public addResponsible() {
    this.data.responsibles.push(
      new FormControl(
        null,
        [
          Validators.required
        ]
      )
    )
  }

  public removeResponsible(index: number) {
    this.data.responsibles.removeAt(index);
  }

}
