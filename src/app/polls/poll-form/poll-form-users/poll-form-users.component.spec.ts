import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollFormUsersComponent } from './poll-form-users.component';

describe('PollFormUsersComponent', () => {
  let component: PollFormUsersComponent;
  let fixture: ComponentFixture<PollFormUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollFormUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollFormUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
