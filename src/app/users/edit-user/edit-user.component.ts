import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/shared/services/users.service';
import { UserProfile } from '../shared/entities/user-profile';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  public constructor(
    private readonly usersService: UsersService,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
    // No action required
  }

  public formSubmit(userProfile: UserProfile) {
    const updateUserSubscription$ = this.usersService.updateUser(userProfile)
      .subscribe({
        next: _ => { this.goBackToUsersList() },
        error: _ => _,
        complete: () => { updateUserSubscription$.unsubscribe() }
      })
  }

  public goBackToUsersList() {
    this.router.navigate(['/users']);
  }

}
