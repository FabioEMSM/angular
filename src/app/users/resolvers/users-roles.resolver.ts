import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { RolesService } from 'src/app/shared/services/roles.service';

@Injectable({
  providedIn: 'root'
})
export class UsersRolesResolver implements Resolve<BasicEntity[]> {

  public constructor(
    private readonly rolesService: RolesService
  ) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<BasicEntity[]> {
    return this.rolesService.getRolesNames();
  }

}
