import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { UsersService } from 'src/app/shared/services/users.service';
import { UserProfile } from '../shared/entities/user-profile';

@Injectable({
  providedIn: 'root'
})
export class UsersProfileResolver implements Resolve<UserProfile> {

  public constructor(
    private readonly usersService: UsersService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserProfile> {
    return this.usersService.getUser(route.params.userId);
  }
}
