import { TestBed } from '@angular/core/testing';

import { UsersParishesResolver } from './users-parishes.resolver';

describe('UsersParishesResolver', () => {
  let resolver: UsersParishesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(UsersParishesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
