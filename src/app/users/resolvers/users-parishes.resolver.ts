import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { ParishesService } from 'src/app/shared/services/parishes.service';

@Injectable({
  providedIn: 'root'
})
export class UsersParishesResolver implements Resolve<BasicEntity[]> {

  public constructor(
    private readonly parishesService: ParishesService
  ) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<BasicEntity[]> {
    return this.parishesService.getFilterParishes([]);
  }

}
