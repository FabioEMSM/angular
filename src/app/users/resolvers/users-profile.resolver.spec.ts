import { TestBed } from '@angular/core/testing';

import { UsersProfileResolver } from './users-profile.resolver';

describe('UsersProfileResolver', () => {
  let resolver: UsersProfileResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(UsersProfileResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
