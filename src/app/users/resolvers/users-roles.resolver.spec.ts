import { TestBed } from '@angular/core/testing';

import { UsersRolesResolver } from './users-roles.resolver';

describe('UsersRolesResolver', () => {
  let resolver: UsersRolesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(UsersRolesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
