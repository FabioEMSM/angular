import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Observable, of, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/helpers/confirm-dialog/confirm-dialog.component';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { ParishesService } from 'src/app/shared/services/parishes.service';
import { UsersService } from 'src/app/shared/services/users.service';
import { Filter } from 'src/app/shared/util/filter';
import { User } from '../shared/entities/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {

  public autoCompleteUsers: Observable<BasicEntity[]> = of([]);

  public autoCompleteParishes: Observable<BasicEntity[]> = of([]);

  public displayedColumns: string[] = [
    'name', 'parish', 'contacts', 'actions'
  ];

  public filterColumns: string[] = this.displayedColumns.map(col => 'filter' + col);

  public usersDataSource: Observable<User[]> = of([]);

  public totalUsers: number = 0;

  public pageIndex: Filter = { name: "page", value: 0 };

  public itemsPerPage: Filter = { name: "itemsPerPage", value: 10 };

  public sortBy: Filter = { name: "sortBy", value: 'name' };

  public sortDirection: Filter = { name: "sortDir", value: 'asc' };

  public filterByName: Filter = { name: "name", value: '' };

  public filterByParish: Filter = { name: "parish", value: '' };

  public formControlNameFilter: FormControl = new FormControl();

  public formControlParishFilter: FormControl = new FormControl();

  private filterNameSubscription$?: Subscription;

  private filterParishSubscription$?: Subscription;

  private usersListSubscription$?: Subscription;

  private removeUserSubscription$?: Subscription;

  private disableUserSubscription$?: Subscription;

  public constructor(
    private readonly usersService: UsersService,
    private readonly parishesService: ParishesService,
    private readonly dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    this.updateTableData();
    this.subscribeFilterNameChanges();
    this.subscribeFilterParishChanges();
  }

  public ngOnDestroy(): void {
    this.filterNameSubscription$?.unsubscribe();
    this.filterParishSubscription$?.unsubscribe();
  }

  private subscribeFilterNameChanges() {
    this.filterNameSubscription$ = this.formControlNameFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this.setFilter(value, this.filterByName)),
        switchMap(value => this.updateNameFilter()),
        map((result: any) => {
          this.autoCompleteUsers = of(result);
        })
      )
      .subscribe();
  }

  private setFilter(value: string | BasicEntity, filter: Filter): string | BasicEntity {
    filter.value = (typeof value === 'string') ? value : value?.name;
    this.updateTableData();

    return value;
  }

  private updateNameFilter(): Observable<BasicEntity[]> {
    return this.usersService.getFilterUsers([this.filterByName]);
  }

  private updateParishFilter(): Observable<BasicEntity[]> {
    return this.parishesService.getFilterUserParishes([this.filterByParish]);
  }

  private subscribeFilterParishChanges() {
    this.filterParishSubscription$ = this.formControlParishFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this.setFilter(value, this.filterByParish)),
        switchMap(value => this.updateParishFilter()),
        map((result: any) => {
          this.autoCompleteParishes = of(result);
        })
      )
      .subscribe();
  }

  private usersListFilters(): Filter[] {
    const filters = [];

    filters.push(this.pageIndex);
    filters.push(this.itemsPerPage);
    filters.push(this.sortBy);
    filters.push(this.sortDirection);
    filters.push(this.filterByName);
    filters.push(this.filterByParish);

    return filters;
  }

  public changePage($page: PageEvent) {
    this.pageIndex.value = $page.pageIndex;
    this.itemsPerPage.value = $page.pageSize;
    this.updateTableData();
  }

  public changeSort($sort: Sort) {
    this.sortBy.value = $sort.active;
    this.sortDirection.value = $sort.direction;
    this.updateTableData();
  }

  public updateTableData() {
    this.usersListSubscription$?.unsubscribe();
    this.usersListSubscription$ = this.usersService
      .getUsers(this.usersListFilters())
      .subscribe({
        next: results => {
          this.usersDataSource = of(results.list);
          this.totalUsers = results.total;
        },
        complete: () => { this.usersListSubscription$?.unsubscribe() }
      });
  }

  public disableUser(user: User) {
    user.enabled = !user.enabled;

    this.disableUserSubscription$?.unsubscribe();
    this.disableUserSubscription$ = this.usersService
      .toggleUserEnabled(user.id)
      .subscribe({
        error: (_: any) => {
          user.enabled = !user.enabled
        },
        complete: () => { this.disableUserSubscription$?.unsubscribe() }
      });
  }

  public confirmUserRemove(user: User) {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        disableClose: true,
        data: {
          title: 'Eliminar utilizador',
          body: 'Tem a certeza de que pretende eliminar o utilizador ' + user.name + '?',
          okButtonAction: () => {
            this.removeUser(user, dialogRef);
          }
        }
      }
    );
  }

  private removeUser(user: User, dialog: MatDialogRef<ConfirmDialogComponent>) {
    this.removeUserSubscription$?.unsubscribe();
    this.removeUserSubscription$ = this.usersService
      .deleteUser(user.id)
      .subscribe({
        next: (result: any) => {
          dialog.close();
          this.updateTableData();
        },
        complete: () => { this.removeUserSubscription$?.unsubscribe() }
      });
  }
}
