import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { BasicEntity } from 'src/app/shared/entities/basic-entity';
import { controlHasErrors } from 'src/app/shared/helper';
import { UsersService } from 'src/app/shared/services/users.service';
import { UserProfile } from '../shared/entities/user-profile';
import { PasswordValidator } from '../../shared/validators/password.validator';
import { UniqueValidator } from '../../shared/validators/unique.validator';
import { nullifyBlankFields } from 'src/app/shared/helper/nullify-blank-fields';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input()
  public isNewUser: boolean = true;

  public roles?: Observable<BasicEntity[]>;

  public parishes?: Observable<BasicEntity[]>;

  public formGroup: FormGroup = new FormGroup({});

  @Output()
  public onSubmit: EventEmitter<UserProfile> = new EventEmitter<UserProfile>();

  @Output()
  public onCancel: EventEmitter<{[_:string]: any}> = new EventEmitter<{[_:string]: any}>();

  constructor(
    private readonly usersService: UsersService,
    private readonly activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // Initialize roles
    this.roles = of(this.activatedRoute.snapshot.data?.roles || []);

    // Initialize parishes
    this.parishes = of(this.activatedRoute.snapshot.data?.parishes || []);

    // Initialize parishes
    const userProfile = this.prepareFormData(this.activatedRoute.snapshot.data?.userProfile || undefined);

    this.initializeForm(userProfile);
  }

  private prepareFormData(userProfile: UserProfile | undefined): any {
    this.isNewUser = userProfile ? false : true;

    return {
      id: userProfile?.id,
      username: userProfile?.username,
      name: userProfile?.name,
      phone: userProfile?.phone,
      email: userProfile?.email,
      enabled: (userProfile?.enabled === undefined) ? true : userProfile?.enabled,
      role: this.getEntityFromObservable(userProfile?.role, this.roles),
      parish: this.getEntityFromObservable(userProfile?.parish, this.parishes)
    };
  }

  private getEntityFromObservable(
    basicEntity?: BasicEntity,
    observable: Observable<BasicEntity[]> = of([])
  ): BasicEntity | undefined {
    let entity = undefined;

    observable.forEach(collection => {
      entity = collection.find(collectionEntity => collectionEntity.id === basicEntity?.id);
    });

    return entity;
  }

  private initializeForm(userProfile: UserProfile) {
    let passwordController: AbstractControl = new FormControl('', [
      this.isNewUser ? Validators.required : (_ => null),
      Validators.minLength(8),
      Validators.maxLength(32),
      PasswordValidator.strength(33.33)
    ]);

    this.formGroup = new FormGroup({
      id: new FormControl(userProfile.id),
      username: new FormControl(
        {
          value: userProfile?.username,
          disabled: !this.isNewUser
        },
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
          Validators.pattern(/^[a-zA-Z0-9\-\._]*$/)
        ],
        [
          UniqueValidator.notUnique(this.usersService, this.usersService.checkUsernameUniqueness)
        ]
      ),
      password: passwordController,
      passwordConfirmation: new FormControl('', [
        this.isNewUser ? Validators.required : (_ => null),
        PasswordValidator.match(passwordController)
      ]),
      name: new FormControl(
        userProfile?.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255)
      ]),
      role: new FormControl(
        userProfile?.role, [
        Validators.required
      ]),
      phone: new FormControl(
        userProfile?.phone, [
        Validators.pattern(new RegExp(/^[0-9]{9,9}$/))
      ]),
      email: new FormControl(
        userProfile?.email, [
        Validators.email
      ]),
      parish: new FormControl(userProfile?.parish, []),
      enabled: new FormControl(userProfile?.enabled, []),
    });
  }

  public hasErrors(formControl: AbstractControl, errors?: string[]) {
    return controlHasErrors(formControl, errors);
  }

  public cancelForm() {
    this.onCancel.emit({ cancel: true });
  }

  public submitForm() {
    this.onSubmit.emit(nullifyBlankFields(this.formGroup.value));
  }

}
