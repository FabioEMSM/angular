import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/shared/services/users.service';
import { UserProfile } from '../shared/entities/user-profile';

@Component({
  selector: 'app-create',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  public constructor(
    private readonly usersService: UsersService,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
  }

  public onSubmit(userProfile: UserProfile) {
    const createuserSubscription$ = this.usersService.createUser(userProfile)
      .subscribe({
        next: _ => { this.goBackToList() },
        error: _ => {},
        complete: () => { createuserSubscription$.unsubscribe(); }
      });
  }

  public goBackToList() {
    this.router.navigate(['/users']);
  }

}
