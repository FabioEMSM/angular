import { BasicEntity } from "src/app/shared/entities/basic-entity";
import { User } from "./user";

export interface UserProfile extends User {
  username: string;
  password?: string;
  passwordConfirmation?: string;
  role: BasicEntity;
}
