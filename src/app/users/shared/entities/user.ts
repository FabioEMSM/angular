import { Parish } from "src/app/parishes/shared/entities/parish";
import { BasicEntity } from "src/app/shared/entities/basic-entity";

export interface User extends BasicEntity {
  email?: string;
  editable: boolean;
  enabled: boolean;
  parish?: Parish;
  phone?: number;
}
