import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../not-found/not-found.component';
import { ManagerAuthGuard } from '../shared/guards/manager-auth.guard';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UsersParishesResolver } from './resolvers/users-parishes.resolver';
import { UsersProfileResolver } from './resolvers/users-profile.resolver';
import { UsersRolesResolver } from './resolvers/users-roles.resolver';
import { UsersListComponent } from './users-list/users-list.component';

const routes: Routes = [
  { path: '',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: UsersListComponent
  },
  { path: 'create',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: CreateUserComponent,
    resolve: {
      roles: UsersRolesResolver,
      parishes: UsersParishesResolver
    }
  },
  { path: ':userId/edit',
    canLoad: [ManagerAuthGuard],
    canActivate: [ManagerAuthGuard],
    canActivateChild: [ManagerAuthGuard],
    component: EditUserComponent,
    resolve: {
      roles: UsersRolesResolver,
      parishes: UsersParishesResolver,
      userProfile: UsersProfileResolver
    }
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
