import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'Autarquicas 2021';

  public constructor(
    private readonly authService: AuthService
  ) {}

  public isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }
}
