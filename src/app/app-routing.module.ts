import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SessionGuard } from './shared/guards/session.guard';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'submit-score', component: NotFoundComponent },
  { path: 'submit-result', component: NotFoundComponent },
  { path: 'validate-results', component: NotFoundComponent },
  { path: 'users',
    canLoad: [SessionGuard],
    canActivate: [SessionGuard],
    canActivateChild: [SessionGuard],
    loadChildren: () => import('./users/users.module').then( m => m.UsersModule )
  },
  { path: 'polls',
    canLoad: [SessionGuard],
    canActivate: [SessionGuard],
    canActivateChild: [SessionGuard],
    loadChildren: () => import('./polls/polls.module').then( m => m.PollsModule )
  },
  { path: 'login', component: LoginFormComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
