export interface TableListing<T> {
  page: number;
  total: number;
  list: T[];
}
