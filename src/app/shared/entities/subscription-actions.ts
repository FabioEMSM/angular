export interface SubscriptionActions {
  next: (_: any) => void,
  error?: (_: any) => void,
  complete?: () => void
}
