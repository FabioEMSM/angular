import { TestBed } from '@angular/core/testing';

import { MediaAuthGuard } from './media-auth.guard';

describe('AuthGuard', () => {
  let guard: MediaAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MediaAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
