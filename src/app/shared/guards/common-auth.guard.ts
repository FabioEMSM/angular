import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AuthRulesEnum } from "../entities/auth-rules.enum";
import { AuthService } from "../services/auth.service";
import { AuthGuard } from "./auth.guard";

@Injectable({
  providedIn: 'root'
})
export class CommonAuthGuard extends AuthGuard {

  protected authRule: AuthRulesEnum = AuthRulesEnum.COMMON;

  public constructor(
    authService: AuthService,
    router: Router
  ) {
    super(authService, router);
  }

}
