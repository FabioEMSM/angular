import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate, CanActivateChild, CanLoad {

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise< UrlTree | boolean > {
    return this.isLoggedIn();
  }

  public canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise< UrlTree | boolean > {
    return this.isLoggedIn();
  }

  public canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Promise< UrlTree | boolean > {
    return this.isLoggedIn();
  }

  private isLoggedIn(): Promise< UrlTree | boolean > {
    if (this.authService.isLoggedIn()) {
      return Promise.resolve(true);
    }

    return this.validateAuthToken();
  }

  private validateAuthToken(): Promise< UrlTree | boolean > {
    return this.authService.refreshToken().toPromise()
      .then(
        success => true,
        error => this.router.parseUrl('/login')
      )
      .catch(error => this.router.parseUrl('/login'));
  }
}
