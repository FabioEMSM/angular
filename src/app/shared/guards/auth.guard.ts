import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import { AuthRulesEnum } from '../entities/auth-rules.enum';
import { AuthService } from '../services/auth.service';

export abstract class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  protected authRule: AuthRulesEnum = AuthRulesEnum.COMMON;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    return this.checkAuthorization();
  }

  public canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    return this.checkAuthorization();
  }

  public canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Promise<boolean | UrlTree> {
    return this.checkAuthorization();
  }

  private checkAuthorization(): Promise< UrlTree | boolean > {
    return Promise.resolve(
      this.authService.getTokenData()?.data?.roleId === this.authRule ?
        true :
        this.router.parseUrl('/')
    );
  }
}
