import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AuthRulesEnum } from "../entities/auth-rules.enum";
import { AuthService } from "../services/auth.service";
import { AuthGuard } from "./auth.guard";

@Injectable({
  providedIn: 'root'
})
export class MediaAuthGuard extends AuthGuard {

  protected authRule: AuthRulesEnum = AuthRulesEnum.MEDIA;

  public constructor(
    authService: AuthService,
    router: Router
  ) {
    super(authService, router);
  }

}
