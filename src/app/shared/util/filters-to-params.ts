import { HttpParams } from "@angular/common/http";
import { Filter } from "./filter";

export class FilterToParams {
  public convertFilters(filters: Filter[]): HttpParams {
    let parameters = new HttpParams();

    for(const filter of filters) {
      parameters = parameters.set(filter.name, filter.value || '');
    }

    return parameters;
  }
}
