import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";
import { passwordStrength } from "src/app/shared/helper";

export class PasswordValidator {
  public static match(passwordController: AbstractControl): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return (control.value && control.value !== passwordController.value) ?
        { match: true } :
        null;
    }
  }

  public static strength(minimumStrength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return (control.value && passwordStrength(control.value) < minimumStrength) ?
        { strength: true } :
        null;
    }
  }
}
