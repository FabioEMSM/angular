import { AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

type Callback = (fn: string) => Observable<any>;
type ObservableReturn = Observable<ValidationErrors | null>;

export class UniqueValidator {
  static notUnique(service: any, callback: Callback): AsyncValidatorFn {
    return (control: AbstractControl): ObservableReturn => (
      control.value ?
        UniqueValidator.callServiceUnique(service, callback, control) :
        of(null)
    )
  }

  private static callServiceUnique(service: any, callback: Callback, control: AbstractControl): ObservableReturn {
    return callback.call(service, control.value)
      .pipe(
        map((result: any) => (result.exists) ? { notunique: true } : null)
      )
  }
}
