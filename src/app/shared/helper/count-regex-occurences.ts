export function countRegExOccurences(regex: RegExp, search: string): number {
  let occurences = 0;

  while (regex.exec(search)) {
    ++occurences;
  }

  return occurences;
}
