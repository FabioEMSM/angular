export function nullifyBlankFields(object: any): any {
  const newObject: any = {};

  for (const param in object) {
    const obj = object[param];
    newObject[param] = (obj !== '') ?
      navigateObject(obj) :
      null;
  }

  return newObject;
}

function navigateObject(object: any) {
  return isObject(object) ?
    nullifyBlankFields(object) :
    object;
}

function isObject(object: any) {
  return !(['string', 'number', 'boolean'].includes(typeof object) || [null, undefined].includes(object) || object instanceof Array);
}
