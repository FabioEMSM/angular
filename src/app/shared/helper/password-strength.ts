import { countRegExOccurences } from "./count-regex-occurences";

export function passwordStrength(password: string) {
  password = password || '';

    // Count repeating characters
    const letters:any = {};
    for (const letter of password) {
      letters[letter] = (letters[letter] || 0) + 1;
    }

    let score = 0;
    for(const letter in letters) {
      score += 5.0 / Math.min(letters[letter], 5);
    }

    // The bigger the entropy, the bigger the score
    score +=
      countRegExOccurences(/\d+/g, password) * 2.5 +
      (
        countRegExOccurences(/[a-z]+/g, password) +
        countRegExOccurences(/[A-Z]+/g, password)
      ) * 1.5 +
      countRegExOccurences(/\w+/g, password) * 4 +
      countRegExOccurences(/\W/g, password) * 5;

    return Math.min(score, 100);
}
