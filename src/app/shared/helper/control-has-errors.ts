import { AbstractControl } from "@angular/forms";

export function controlHasErrors(formControl: AbstractControl, errors?: string[]) {
  if (formControl.untouched || formControl.valid) {
    return false;
  }

  for (const error of errors || []) {
    if (formControl.hasError(error)) {
      return true;
    }
  }

  return !errors?.length;
}
