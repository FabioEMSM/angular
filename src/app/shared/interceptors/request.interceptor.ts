import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment"
import { AuthService } from "../services/auth.service";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  public constructor(
    private readonly authService: AuthService
  ) {}

  public intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = httpRequest.url;

    return next.handle(
        this.isBackendRequest(url) ?
          this.transformRequest(httpRequest) :
          httpRequest
      );
  }

  private transformRequest(httpRequest: HttpRequest<any>) {
    const url = httpRequest.url;
    const token = this.authService.getToken();

    return httpRequest.clone({
      url: this.buildURL(url),
      withCredentials: true,
      headers: token ?
        httpRequest.headers.set('Authorization', 'Bearer ' + this.authService.getToken()) :
        httpRequest.headers
    });
  }

  private isBackendRequest(url: string): boolean {
    return  !url.startsWith('http://') &&
            !url.startsWith('https://') &&
            !url.startsWith('assets/') &&
            !url.startsWith('/assets/');
  }

  private buildURL(url: string): string {
    return (environment.useSSL ? 'https://' : 'http://') +
      environment.url + ':' + environment.port + '/' + url.replace(/^\//g, '');
  }
}
