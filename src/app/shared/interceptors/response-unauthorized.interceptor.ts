import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthService } from "../services/auth.service";

@Injectable()
export class ResponseUnauthorizedInterceptor implements HttpInterceptor {

  public constructor(
    private readonly authService: AuthService
  ) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => this.validateErrorAndRethrow(request, next, error))
    );
  }

  private validateErrorAndRethrow(request: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): Observable<any> {
      this.refreshTokenIfUnauthorized(request, next, error);
      throw error;
  }

  private refreshTokenIfUnauthorized(request: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse) {
    if (error.status === 401 && !error.url?.endsWith('/api/auth/refresh')) {
      const subscription$ = this.authService.refreshToken().subscribe({
        next: () => { next.handle(request) },
        complete: () => { subscription$.unsubscribe() }
      });
    }
  }
}
