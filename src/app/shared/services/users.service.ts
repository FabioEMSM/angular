import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserProfile } from 'src/app/users/shared/entities/user-profile';
import { TableListing } from 'src/app/shared/entities/table-listing';
import { BasicEntity } from '../entities/basic-entity';
import { Filter } from '../util/filter';
import { FilterToParams } from '../util/filters-to-params';
import { User } from 'src/app/users/shared/entities/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends FilterToParams {

  constructor(
    private readonly http: HttpClient
  ) {
    super();
  }

  public createUser(userProfile: UserProfile): Observable<any> {
    return this.http.post<any>('/api/users', userProfile);
  }

  public getUser(userId: number): Observable<UserProfile> {
    return this.http.get<UserProfile>('/api/users/' + userId);
  }

  public updateUser(userProfile: UserProfile): Observable<any> {
    return this.http.put<any>('api/users/' + userProfile.id, userProfile);
  }

  // TODO: change 'any' type
  public deleteUser(userId: number): Observable<any> {
    return this.http.delete<any>('api/users/' + userId);
  }

  public getUsers(filters: Filter[] = []): Observable<TableListing<User>> {
    const params = this.convertFilters(filters);

    return this.http.get<TableListing<User>>('api/users', { params });
  }

  // TODO: change 'any' type
  public toggleUserEnabled(userId: number): Observable<any> {
    return this.http.patch<any>('api/users/' + userId + '/toggle', null);
  }

  public getFilterUsers(filter: Filter[]): Observable<BasicEntity[]> {
    const params = this.convertFilters(filter);
    return this.http.get<BasicEntity[]>('api/users/filter', { params });
  }

  // TODO: change 'any' type
  public checkUsernameUniqueness(search: string): Observable<any> {
    const params = this.convertFilters([{ name: 'username', value: search }]);
    return this.http.get<any>('api/users/unique', { params });
  }
}
