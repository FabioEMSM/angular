import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { interval, Observable, Subject } from "rxjs";
import { startWith } from "rxjs/operators";
import { SubscriptionActions } from "../entities/subscription-actions";

@Injectable({
  providedIn: 'root'
})
export class ScoreResultsService {
  private scoresSubcribable$ = new Subject<{ newScores: number }>();
  private newScoresSubscriber$: Observable<number>|undefined;

  public constructor(
    private readonly http: HttpClient
  ) {}

  private initMulticastScore() {
    if (!this.newScoresSubscriber$) {
      this.newScoresSubscriber$ = interval(10000).pipe(startWith(0));
      this.newScoresSubscriber$.subscribe({
        next: (_: any) => {
          const thisObservable: any = this.http.get<{newScores: number}>('assets/newScore.json')
            .subscribe({
              next: score => {
                this.scoresSubcribable$.next(score);
              },
              complete: () => thisObservable.unsubscribe()
            });
        }
      });
    }
  }

  public subscribeScoreUpdates(
    actions: SubscriptionActions
  ): any {
    this.initMulticastScore();

    return this.scoresSubcribable$.subscribe(actions);
  }
}
