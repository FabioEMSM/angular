import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PartyFilter } from 'src/app/parties/shared/entities/party-filter';
import { Filter } from '../util/filter';
import { FilterToParams } from '../util/filters-to-params';

@Injectable({
  providedIn: 'root'
})
export class PartiesService extends FilterToParams {

  constructor(
    private readonly http: HttpClient
  ) {
    super();
  }

  public getFilterParties(filter: Filter[]): Observable<PartyFilter[]> {
    const params = this.convertFilters(filter);
    return this.http.get<PartyFilter[]>('assets/parties-filter.json', { params });
  }
}
