import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { shareReplay, tap } from 'rxjs/operators';

type UserCredentials = {
  username: string,
  password: string
}

type TokenData = {
  idToken?: string;
  expiration?: Date;
  data?: TokenUserData;
};

type TokenUserData = {
  exp: number;
  name: string;
  sub: string;
  roleId: number;
  roleName: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private canRefreshToken: boolean = true;

  private isSessionRefreshed: boolean = false;

  private memoryToken: TokenData = {};

  public constructor(private readonly http: HttpClient) { }

  public login(user: UserCredentials) {
    return this.authRequest('/api/auth/login', user);
  }

  public refreshToken() {
    return this.authRequest('/api/auth/refresh');
  }

  private authRequest(url: string, user?: UserCredentials) {
    return this.http.post<any>(url, user, { observe: 'response' })
      .pipe(
        tap((result: HttpResponse<any>) => {
          this.canRefreshToken = true;
          this.isSessionRefreshed = true;
          this.setSession(result);
        }),
        shareReplay()
      );
  }

  private setSession(authResult: HttpResponse<any>) {
    this.memoryToken.idToken = authResult.headers.get('Auth-token')?.split(' ')[1];
    this.memoryToken.expiration = new Date(authResult.headers.get('Auth-expire') || '');

    if(this.memoryToken.idToken) {
      this.decodeAndSetJSONPayload();
    }
  }

  private decodeAndSetJSONPayload() {
    const base64 = (this.memoryToken.idToken?.split('.')[1] || '')
      .replace(/-/g, '+')
      .replace(/_/g, '/');

    this.memoryToken.data = JSON.parse(
      decodeURIComponent(
        atob(base64)
          .split('')
          .map(char => {
            return '%' + ('00' + char.charCodeAt(0).toString(16)).slice(-2);
          })
          .join('')
      )
    );
  }

  public logout() {
    return this.http.post<any>('/api/auth/logout', null)
      .pipe(
        tap(() => {
          this.memoryToken.idToken = undefined;
          this.memoryToken.expiration = undefined;
          this.memoryToken.data = undefined;
        })
      );
  }

  public isLoggedIn(): boolean {
    // Test for expiration date if there is one and if it fails
    return +(this.getExpiration()) > +(new Date()) ||
      // Test if a session is opened in the backend
      this.refreshTokenLoggedInStatus();
  }

  private refreshTokenLoggedInStatus(): boolean {
    if (this.canRefreshToken) {
      this.canRefreshToken = false;
      this.refreshToken().toPromise().then(
        _ => _,
        _ => { this.canRefreshToken = false; this.isSessionRefreshed = false; }
      );
    }

    return this.isSessionRefreshed;
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  public getExpiration(): Date {
    return this.memoryToken.expiration || new Date();
  }

  public getToken(): string | undefined {
    return this.isLoggedIn() ? this.memoryToken.idToken : undefined;
  }

  public getTokenData(): TokenData {
    return this.memoryToken;
  }
}
