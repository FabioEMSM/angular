import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BasicEntity } from '../entities/basic-entity';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  public constructor(
    private readonly http: HttpClient
  ) { }

  public getRolesNames(): Observable<BasicEntity[]> {
    return this.http.get<BasicEntity[]>('api/roles');
  }

}
