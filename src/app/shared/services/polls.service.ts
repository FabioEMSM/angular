import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Poll } from 'src/app/polls/shared/entities/poll';
import { BasicEntity } from '../entities/basic-entity';
import { TableListing } from '../entities/table-listing';
import { Filter } from '../util/filter';
import { FilterToParams } from '../util/filters-to-params';

@Injectable({
  providedIn: 'root'
})
export class PollsService extends FilterToParams {

  public constructor(
    private readonly http: HttpClient
  ) {
    super();
  }

  public deletePoll(pollId: number): Observable<any> {
    return this.http.delete<any>('polls/' + pollId);
  }

  public getPolls(filters: Filter[]): Observable<TableListing<Poll>> {
    const params = this.convertFilters(filters);

    return this.http.get<TableListing<Poll>>('assets/polls-list.json', { params });
  }

  public getFilterPolls(filter: Filter): Observable<BasicEntity> {
    const params = this.convertFilters([filter]);
    return this.http.get<BasicEntity>('assets/polls-filter.json', { params });
  }

  // TODO: change 'any' type
  public checkPollUniqueness(search: string): Observable<any> {
    const params = this.convertFilters([{ name: 'poll', value: search }]);
    return this.http.get<any>('assets/unique-poll.json', { params });
  }

}
