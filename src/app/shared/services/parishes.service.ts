import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Parish } from 'src/app/parishes/shared/entities/parish';
import { Filter } from '../util/filter';
import { FilterToParams } from '../util/filters-to-params';

@Injectable({
  providedIn: 'root'
})
export class ParishesService extends FilterToParams {

  constructor(
    private readonly http: HttpClient
  ) {
    super();
  }

  public getFilterUserParishes(filter: Filter[]): Observable<Parish[]> {
    const params = this.convertFilters(filter);
    return this.http.get<Parish[]>('api/parishes/users/filter', { params });
  }

  public getFilterParishes(filter: Filter[]): Observable<Parish[]> {
    const params = this.convertFilters(filter);
    return this.http.get<Parish[]>('api/parishes', { params });
  }
}
