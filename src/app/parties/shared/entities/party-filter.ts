import { BasicEntity } from "src/app/shared/entities/basic-entity";

export interface PartyFilter extends BasicEntity {
  short: string;
  color: string;
}
