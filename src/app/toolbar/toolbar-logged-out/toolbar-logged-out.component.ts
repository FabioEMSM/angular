import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar-logged-out',
  templateUrl: './toolbar-logged-out.component.html',
  styleUrls: ['./toolbar-logged-out.component.scss']
})
export class ToolbarLoggedOutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
