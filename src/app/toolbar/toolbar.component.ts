import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output()
  public menuClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  public constructor(
    private readonly authService: AuthService
  ) {
    // No action required
  }

  public ngOnInit(): void {
    // No action required
  }

  public isUserLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

}
