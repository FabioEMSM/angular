import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ScoreResultsService } from 'src/app/shared/services/score-results.service';

@Component({
  selector: 'app-toolbar-logged-in',
  templateUrl: './toolbar-logged-in.component.html',
  styleUrls: ['./toolbar-logged-in.component.scss']
})
export class ToolbarLoggedInComponent implements OnInit {

  public isThereNewScoreResults: boolean = false;

  private scoreResultsSubscription$: any;

  public name?: String;

  @Output()
  public menuClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  public constructor(
    private readonly scoreResults: ScoreResultsService,
    private readonly authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.scoreResultsSubscription$ = this.scoreResults.subscribeScoreUpdates({
      next: ((results: {newScores: number}) => {
        this.isThereNewScoreResults = !!results.newScores;
      })
    });

    this.name = this.authService.getTokenData().data?.name;
  }

  public ngOnDestroy() {
    this.scoreResultsSubscription$.unsubscribe();
  }

  public toogleMenu(): void {
    this.menuClick.emit(true);
  }

}
