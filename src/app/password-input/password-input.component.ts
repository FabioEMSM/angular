import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';
import { passwordStrength } from '../shared/helper';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.scss'],
  providers: []
})
export class PasswordInputComponent implements OnInit, ControlValueAccessor {

  @Input()
  public required:boolean = false;

  public isDisabled:boolean = false;

  public isMouseDown:boolean = false;

  public isTouched:boolean = false;

  public onChangeAction: any;

  private onTouchAction: any;

  private fallbackController: FormControl = new FormControl();

  public constructor(
    public ngControl: NgControl
  ) {
    ngControl.valueAccessor = this;
  }

  public ngOnInit(): void {
  }

  public controller (): FormControl {
    return (this.ngControl.control || this.fallbackController) as FormControl;
  }

  public writeValue(obj: string): void {
    this.controller().setValue(obj);
  }

  public registerOnChange(fn: any): void {
    this.onChangeAction = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouchAction = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.controller().disable();
    }
    else {
      this.controller().enable();
    }
  }

  public onTouch($event: any) {
    if (!this.isTouched) {
      this.onTouchAction($event);
    }
  }

  public getStrength() {
    return passwordStrength(
      this.controller().value
    );
  }

}
