import { BasicEntity } from "src/app/shared/entities/basic-entity";

export interface Parish extends BasicEntity {
  short: string;
}
