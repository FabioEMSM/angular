import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { BasicEntity } from '../shared/entities/basic-entity';

@Component({
  selector: 'app-autocomplete-filter',
  templateUrl: './autocomplete-filter.component.html',
  styleUrls: ['./autocomplete-filter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: AutocompleteFilterComponent
    }
  ]
})
export class AutocompleteFilterComponent implements OnInit, ControlValueAccessor {

  @Input()
  public label: string = '';

  @Input()
  public options: Observable<BasicEntity[]> = of([]);

  public formControl: FormControl = new FormControl();

  public inputValue?: string;

  public isTouched: boolean = false;

  private onChange?: any;

  private onTouched?: any;

  private onChangeSubscription$?: Subscription;

  public constructor() { }

  public writeValue(value: string): void {
    this.inputValue = value;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.formControl.disable()
    }
    else {
      this.formControl.enable();
    }
  }

  public ngOnInit(): void {
    this.subscribeOnChange();
  }

  public ngOnDestroy(): void {
    this.onChangeSubscription$?.unsubscribe();
  }

  private subscribeOnChange() {
    this.onChangeSubscription$ = this.formControl.valueChanges.subscribe({
      next: value => (this.onChange ? this.onChange(value) : null)
    });
  }

  public onTouch() {
    if (!this.isTouched) {
      this.isTouched = true;
      if (this.onTouched) {
        this.onTouched();
      }
    }
  }

  public displayFunction(option: BasicEntity) {
    return option?.name;
  }
}
