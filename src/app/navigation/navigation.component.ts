import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { AuthRulesEnum } from '../shared/entities/auth-rules.enum';
import { AuthService } from '../shared/services/auth.service';
import { ScoreResultsService } from '../shared/services/score-results.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {

  public newResultsForValidation: number = 0;

  private scoreResultsSubscription$: any;

  @Output()
  public close: EventEmitter<boolean> = new EventEmitter<boolean>();

  public constructor(
    private readonly scoreResults: ScoreResultsService,
    private readonly authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.scoreResultsSubscription$ = this.scoreResults.subscribeScoreUpdates({
      next: ((results: {newScores: number}) => {
        this.newResultsForValidation = results.newScores || 0;
      })
    });
  }

  public ngOnDestroy() {
    this.scoreResultsSubscription$.unsubscribe();
  }

  public closeNavigation() {
    this.close.emit(true);
  }

  public isManager() {
    return this.authService.getTokenData()?.data?.roleId === AuthRulesEnum.MANAGER;
  }

  public logout(): void {
    const logoutSubscription$ = this.authService.logout().subscribe({
      next: () => {
        // Reload page to fully logout
        window.location.reload();
      },
      complete: () => { logoutSubscription$.unsubscribe() }
    });
  }

}
