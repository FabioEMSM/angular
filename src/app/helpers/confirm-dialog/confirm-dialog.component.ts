import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export type ConfirmDialogData = {
  title: string,
  body: string,
  okButton?: string,
  cancelButton?: string,
  okButtonValue?: boolean,
  cancelButtonValue?: boolean,
  okButtonAction?: () => void,
  cancelButtonAction?: () => void
};

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    private readonly dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: ConfirmDialogData
  ) { }

  ngOnInit(): void {
  }

}
