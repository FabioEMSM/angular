export const environment = {
  production: true,
  useSSL: true,
  url: 'localhost',
  port: 8444
};
